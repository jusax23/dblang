# DBlang

[![status-badge](https://ci.jusax.de/api/badges/jusax23/dblang/status.svg)](https://ci.jusax.de/jusax23/dblang)

TypeScript Libary for relational Database Requests. Requests are written in js/ts and it supports automatic Schema evulution.

Features:
- [x] select Query
- [x] insert Query
- [x] update Query
- [x] remove Query
- [x] create Schema
- [x] remove unused Schema
- [x] joins
- [x] table alias
- [x] general aliases
- [x] unified response
- [ ] change Schema
- [ ] Key-Value Store Shortcut
- [ ] Views

Supported Databses:
- [x] mariadb
- [ ] mysql (not tested)
- [ ] postrges

## Getting Started

### Connect

```javascript
import { DB } from "dblang";
const db = new DB();
db.connect({
    host: "localhost",
    user: "root",
    password: "0123456789",
    database: "databaseName" 
});

```

### Schema

Tables:
```javascript
const Table1 = db.newTable("Table1Name");
const Table2 = db.newTable("Table2Name");
```

Add Attributes:
```javascript
Table1.addAttribute("AttrName", TYPE, {
    default: 5,
    // other settings
});

Table2.addAttributes({
    "AttrName2":{
        type: TYPE,
        default: 6
        // other settings
    },
    "AttrName3":{
        type: TYPE,
        default: 6
        // other settings
    },
    // more Attributes
});
```

TYPE: See [Types](#types)

Other Settings:
- `unique: boolean`: (optional) Add Unique Constraint
- `autoIncrement: boolean`: (optional) Set auto Increment (must be primary key)
- `default: any`: (optional) Set default Property.
- `notNull: boolean`: (optional) Set not nullable.
- `primaryKey: boolean`: (optinal) mark as part of primary Key.
- `foreignKey: Object`: Add foreign Constraint for single Attribute.
    - `link: Attribute`: Linked Attribute
    - `onDelete: onAction`: Specify delete Action [onAction](#onaction)
    - `onUpdate: onAction`: Specify update Action [onAction](#onaction)
- `check: BooleanModifier | ((a: Attribute) => BooleanModifier)`: Add check Constraint (A function must be used when check references its self.)

Sync: 
Create Tables, Attributes and Constraints
```javascript
await db.sync();

await db.sync(true); //delete unused Elements (May do a backup befor.)
```

### Querys
See: [Modifiers](#modifier), [Aggregations](#aggregations)

#### Select:
```javascript
import { select } from "dblang"

let res = await select([Table2.AttrName2], Table1)
    .where(eq(Table2.AttrName2, 5)) //optional
    .groupBy(Table2.AttrName3) //optional
    .having(le(Table2.AttrName3, 5)) //optional
    .limit(10) //optional
    .query(db);
```

_Joins_:
```javascript
import { INT, onAction } from "dblang"

const TableA = db.newTable("TableA");
const TableB = db.newTable("TableB");
TableA.addAttribute("A1", INT, {
    primaryKey: true
});
TableB.addAttribute("B1", INT, {
    primaryKey: true,
    foreignKey:{
        link: TableA.A1,
        onDelete: onAction.cascade
    }
});
let res = await select([TableA.A1, TableB.B1], innerJoinOn(eq(TableA.A1, Tableb.B1)))
    .query(db);
```
See: [Joins](#joins)

_Aliases_:
```javascript 
const alias1 = TableA.createAlias("alias1");
//alias1 can be used like a Table in select Statements.
const alias2 = alias(avg(TableA.A1), "someAvg");
```

Get requestet Data:
```javascript
let response = await select([TableA.A1, TableB.B1], innerJoinOn(eq(TableA.A1, Tableb.B1)))
    .query(db);

response[0][TableA.A1];
response[0][TableB.B1];

response[i][TableA.A1];
response[i][TableB.B1]; // i < repsonse.length

```

#### Insert:
```javascript
import { select } from "dblang"

await insert(Table2.AttrName2, Table2.AttrName3)
    .add(1, 2)
    .add(2, 3)
    .addValues([4, 5], [3, 4])
    .query(db);

await insert(Table2.AttrName2, Table2.AttrName3)
    .setSelect(select(Table1.AttrName, 4))
    .query(db);
```

#### Update:
```javascript
import { update } from "dblang"

await update(Table2)
    .set(Table2.AttrName2, plus(Table2.AttrName2,1))
    .set(Table2.AttrName3, minus(Table2.AttrName2,1))
    .where(leq(Table2.AttrName2, 5))
    .query(db);
```

#### Remove:
```javascript
import { remove } from "dblang"

await remove(Table2)
    .where(geq(Table2.AttrName2, 5))
    .query(db);
```

### Content List
#### Types

- `CHAR(size)`
- `VARCHAR(size)`
- `BINARY(size)`
- `VARBINARY(size)`
- `TINYBLOB`
- `BLOB`
- `MEDIUMBLOB`
- `LONGBLOB`
- `TINYTEXT`
- `TEXT`
- `MEDIUMTEXT`
- `LONGTEXT`
- `ENUM(val1, val2, ...)`
- `SET(val1, val2, ...)`
- `BOOL`
- `BIT`
- `TINYINT`
- `SMALLINT`
- `MEDIUMINT`
- `INT`
- `BIGINT`
- `FLOAT(size, d)`
- `DOUBLE(size, d)`
- `DECIMAL(size, d)`
- `DATE`
- `DATETIME`
- `TIMESTAMP`
- `TIME`
- `YEAR`

#### onAction

Action for onDelete or onUpdate.

Usage:
```javascript
import { onAction } from "dblang"

onAction.cascade;
onAction.noAction; // dangerous
onAction.setNull;
onAction.setDefault;
```

#### Modifier
BooleanModifier: 
- `and(v1, v2, ...)`
- `or(v1, v2, ...)`
- `ge(v1, v2, ...)`
- `geq(v1, v2, ...)`
- `eq(v1, v2, ...)`
- `leq(v1, v2, ...)`
- `le(v1, v2, ...)`
- `not(v1)`
- `like(v1, v2)`
- `exists(s)`

NumberModifier:
- `plus(v1, v2, ...)`
- `minus(v1, v2, ...)`
- `mult(v1, v2, ...)`
- `divide(v1, v2, ...)`

StringModifier:
- `concat(v1, v2, ...)`

(v\* = string, number, boolean, null, Modifier, Aggregation, select Query, Attribute; s = selectQuery)

#### Joins

- `crossJoin(t1, t2, ...)`
- `naturalJoin(t1, t2, ...)`
- `leftNaturalJoin(t1, t2, ...)`
- `rightNaturalJoin(t1, t2, ...)`
- `fullNaturalJoin(t1, t2, ...)`
- `innerJoinUsing(t1, t2, a1, a2, ...)`
- `leftJoinUsing(t1, t2, a1, a2, ...)`
- `rightJoinUsing(t1, t2, a1, a2, ...)`
- `fullJoinUsing(t1, t2, a1, a2, ...)`
- `innerJoinOn(t1, t2, b)`
- `leftJoinOn(t1, t2, b)`
- `rightJoinOn(t1, t2, b)`
- `fullJoinOn(t1, t2, b)`

(t\* = Table or Join; a\* = Attributes; b = Boolean Modifier)

#### Aggregations
- `count(a)`
- `sum(a)`
- `avg(a)`
- `min(a)`
- `max(a)`

(a = Attribute)