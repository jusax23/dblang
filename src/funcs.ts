import { Alias } from "./alias.js";
import { Attribute, Table } from "./db.js";
import { Aggregation, BooleanModifier, checkConstraint, Datatype, foreignConstraint, joinCross, joinNatural, usingJoin, onJoin, NumberModifier, StringModifier, uniqueConstraint } from "./dbStructure.js";
import { insertQuery, removeQuery, selectQuery, updateQuery } from "./query.js";
import { allModifierInput, joinType, selectElements, selectFromElements } from "./types.js";

//modifiers
export const and = (...args: BooleanModifier[]) => new BooleanModifier("and", args);
export const or = (...args: BooleanModifier[]) => new BooleanModifier("or", args);
export const ge = (...args: allModifierInput[]) => new BooleanModifier("ge", args);
export const geq = (...args: allModifierInput[]) => new BooleanModifier("geq", args);
export const eq = (...args: allModifierInput[]) => new BooleanModifier("eq", args);
export const leq = (...args: allModifierInput[]) => new BooleanModifier("leq", args);
export const le = (...args: allModifierInput[]) => new BooleanModifier("le", args);
export const plus = (...args: allModifierInput[]) => new NumberModifier("plus", args);
export const minus = (...args: allModifierInput[]) => new NumberModifier("minus", args);
export const mult = (...args: allModifierInput[]) => new NumberModifier("mult", args);
export const divide = (...args: allModifierInput[]) => new NumberModifier("divide", args);

export const not = (arg: allModifierInput) => new BooleanModifier("not", [arg]);
export const like = (a: allModifierInput, b: allModifierInput) => new BooleanModifier("like", [a, b]);
export const regexp = (a: allModifierInput, b: allModifierInput) => new BooleanModifier("regexp", [a, b]);

export const exists = (q: selectQuery) => new BooleanModifier("exists", [q]);

export const concat = (...args: allModifierInput[]) => new StringModifier("concat", args);
export const coalesce = (...args: allModifierInput[]) => new StringModifier("coalesce", args);

//aggregations
export const count = (a: Attribute) => new Aggregation("count", a);
export const sum = (a: Attribute) => new Aggregation("sum", a);
export const avg = (a: Attribute) => new Aggregation("avg", a);
export const min = (a: Attribute) => new Aggregation("min", a);
export const max = (a: Attribute) => new Aggregation("max", a);

//join

export const crossJoin = (...tables: Table[]) => new joinCross(tables);

export const naturalJoin = (...tables: Table[]) => new joinNatural(tables, joinType.inner);
export const leftNaturalJoin = (...tables: Table[]) => new joinNatural(tables, joinType.left);
export const rightNaturalJoin = (...tables: Table[]) => new joinNatural(tables, joinType.right);
export const fullNaturalJoin = (...tables: Table[]) => new joinNatural(tables, joinType.full);

export const innerJoinUsing = (tableA: Table, tableB: Table, ...using: Attribute[]) => new usingJoin(tableA, tableB, joinType.inner, using);
export const leftJoinUsing = (tableA: Table, tableB: Table, ...using: Attribute[]) => new usingJoin(tableA, tableB, joinType.left, using);
export const rightJoinUsing = (tableA: Table, tableB: Table, ...using: Attribute[]) => new usingJoin(tableA, tableB, joinType.right, using);
export const fullJoinUsing = (tableA: Table, tableB: Table, ...using: Attribute[]) => new usingJoin(tableA, tableB, joinType.full, using);

export const innerJoinOn = (tableA: Table, tableB: Table, on: BooleanModifier) => new onJoin(tableA, tableB, joinType.inner, on);
export const leftJoinOn = (tableA: Table, tableB: Table, on: BooleanModifier) => new onJoin(tableA, tableB, joinType.left, on);
export const rightJoinOn = (tableA: Table, tableB: Table, on: BooleanModifier) => new onJoin(tableA, tableB, joinType.right, on);
export const fullJoinOn = (tableA: Table, tableB: Table, on: BooleanModifier) => new onJoin(tableA, tableB, joinType.full, on);

//query
export const select = (args: selectElements[], from: selectFromElements) => new selectQuery(args, from);
export const insert = (...attrs: Attribute[]) => new insertQuery(attrs);
export const update = (table: Table) => new updateQuery(table);
export const remove = (table: Table) => new removeQuery(table);

//alias
export const alias = (a: selectElements, name: string) => new Alias(a, name);

//datatypes

export const CHAR = (size: number) => new Datatype("char", [size]);
export const VARCHAR = (size: number) => new Datatype("varchar", [size]);
export const BINARY = (size: number) => new Datatype("binary", [size]);
export const VARBINARY = (size: number) => new Datatype("varbinary", [size]);

export const TINYBLOB = new Datatype("tinyblob", []);
export const BLOB = new Datatype("blob", []);
export const MEDIUMBLOB = new Datatype("mediumblob", []);
export const LONGBLOB = new Datatype("longblob", []);

export const TINYTEXT = new Datatype("tinytext", []);
export const TEXT = new Datatype("text", []);
export const MEDIUMTEXT = new Datatype("mediumtext", []);
export const LONGTEXT = new Datatype("longtext", []);

export const ENUM = (...values: string[]) => new Datatype("enum", values);
export const SET = (...values: string[]) => new Datatype("set", values);

export const BOOL = new Datatype("bool", []);
export const BIT = new Datatype("bit", []);
export const TINYINT = new Datatype("tinyint", []);
export const SMALLINT = new Datatype("smallint", []);
export const MEDIUMINT = new Datatype("mediumint", []);
export const INT = new Datatype("int", []);
export const BIGINT = new Datatype("bigint", []);

export const FLOAT = (size: number, d: number) => new Datatype("float", [size, d]);
export const DOUBLE = (size: number, d: number) => new Datatype("double", [size, d]);
export const DECIMAL = (size: number, d: number) => new Datatype("decimal", [size, d]);

export const DATE = new Datatype("date", []);
export const DATETIME = new Datatype("datetime", []);
export const TIMESTAMP = new Datatype("timestamp", []);
export const TIME = new Datatype("time", []);
export const YEAR = new Datatype("year", []);

// Constraints
//TODO:
//primary key
export const foreignKey = (attrs: Attribute[], target: Attribute[]) => new foreignConstraint(
    "foreign_constraint_"
    + attrs.map(a => a.name + "_" + a.table.dbLangTableName).join("_")
    + "_to_"
    + target.map(a => a.name + "_" + a.table.dbLangTableName).join("_"),
    attrs, target);
export const uniqueKey = (attrs: Attribute[]) => new uniqueConstraint("unique_constraint_" + attrs.map(a => a.name + "_" + a.table.dbLangTableName).join("_"), attrs);
export const check = (name: string, mod: BooleanModifier) => new checkConstraint(name, mod);


/**
 * primary key: kein richtiger Constraint -> renew = drop
 * foreign key: ?
 * unique: richtiger Constraint -> achtung manchmal nicht entfernabr
 * check: richtiger Constraint
 */