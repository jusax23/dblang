import { AttributeAlias } from "./alias.js";
import { Attribute } from "./db.js";
import { primaryData } from "./types.js";

export class singleResponse {
    [key: string]: primaryData | any;
    constructor(r: object) {
        Object.assign(this, r);
    }
    getVal(a: Attribute | AttributeAlias) {
        return this[a.toString()];
    }
}

export class readResponse extends Array<singleResponse>{
    constructor(rows: ({ [key: string]: primaryData })[]) {
        super();
        for(let i = 0; i < rows.length; i++){
            this[i] = new singleResponse(rows[i])
        }
    }
}

export class writeResponse {
    affectedRows: number;
    constructor(affectedRows: number) {
        this.affectedRows = affectedRows;
    }
}

export class insertResponse extends writeResponse {
    insertId: number;
    constructor(affectedRows: number, insertId: number) {
        super(affectedRows);
        this.insertId = insertId;
    }
}
