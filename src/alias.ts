import { Attribute, DB, Table } from "./db.js"
import { Aggregation, Modifier } from "./dbStructure.js";
import { Handler } from "./defaultHandler.js";
import { QueryBuilder, selectQuery } from "./query.js";
import { selectElements } from "./types.js";

export class AttributeAlias {
    name: string;
    nameO: string;
    table: TableAlias;
    attr: Attribute;
    constructor(attr: Attribute, table: TableAlias) {
        this.name = attr.name;
        this.nameO = attr.nameO;
        this.attr = attr;
        this.table = table;
    }
    serialize(handler: Handler) {
        return handler.builders.escapeID(this.name);
    }
    getString(handler: Handler = this.table.dbLangDatabaseInstance.getHandler()) {
        return this.table.serialize(handler) + "." + this.serialize(handler);
    }
    getStringFunc(handler: Handler) {
        return this.table.serialize(handler) + "(" + this.serialize(handler) + ")";
    }
    toString() {
        return this.table.dbLangTableName + "_" + this.name;
    }
}

export class TableAlias {
    dbLangTableName: string;
    dbLangTableAttributes: { [key: string]: AttributeAlias; } = {};
    dbLangDatabaseInstance: DB;
    dbLangTableInstance: Table;
    [key: string]: AttributeAlias | any;
    constructor(name: string, table: Table) {
        this.dbLangTableName = name;
        this.dbLangTableInstance = table;
        this.dbLangDatabaseInstance = table.dbLangDatabaseInstance;
        let keys = Object.keys(table.dbLangTableAttributes);
        for (let i = 0; i < keys.length; i++) {
            const a = table.dbLangTableAttributes[keys[i]];
            let attrAlias = new AttributeAlias(a, this);
            this[a.name] = attrAlias;
            this[a.nameO] = attrAlias;
            this.dbLangTableAttributes[a.name] = attrAlias;
        }
    }
    serializeAlias(hander: Handler) {
        return this.dbLangTableInstance.serialize(hander) + " " + this.toString(hander);
    }
    serialize(handler: Handler) {
        return this.toString(handler);
    }
    toString(handler: Handler = this.dbLangDatabaseInstance.getHandler()) {
        return handler.builders.escapeID(this.dbLangTableName);
    }
}

export class Alias {
    alias: string;
    a: selectElements;

    constructor(a: selectElements, alias: string) {
        this.alias = alias;
        this.a = a;
    }

    serialize(handler: Handler) {
        let builder = new QueryBuilder();
        if (this.a instanceof Attribute || this.a instanceof AttributeAlias)
            builder.addCode(this.a.getString(handler) + " " + handler.builders.escapeID(this.a.toString()));
        else if (this.a instanceof Modifier || this.a instanceof selectQuery || this.a instanceof Aggregation || this.a instanceof Alias) {
            builder.addCode("(");
            builder.append(this.a.serialize(handler));
            builder.addCode(")");
        } else {
            builder.addInjection(this.a);
        }
        return builder;
    }
    getString(handler: Handler) {
        return this.serialize(handler);
    }
    toString() {
        return this.alias;
    }
}