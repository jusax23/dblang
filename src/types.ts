import { Alias, AttributeAlias, TableAlias } from "./alias.js";
import { Attribute, Table } from "./db.js";
import { Aggregation, BooleanModifier, Datatype, Joins, Modifier } from "./dbStructure.js";
import { QueryBuilder, selectQuery } from "./query.js";

export type primaryData = string | number | boolean | null;
export type allModifierInput = primaryData | Modifier | selectQuery | Attribute | AttributeAlias | Aggregation | Alias;

export type selectElements = primaryData | Attribute | AttributeAlias | Aggregation | selectQuery | Alias;
export type selectFromElements = TableAlias | Table | Joins | null;
export type joinElements = Table | Joins;

export type serializeReturn = [string, primaryData[]];

export type DatatypeBuild = [QueryBuilder, number];

export type attributeSettings = {
    unique?: boolean,
    autoIncrement?: boolean,
    default?: primaryData,
    notNull?: boolean
    primaryKey?: boolean,
    foreignKey?: {
        link: Attribute,
        onDelete?: onAction,
        onUpdate?: onAction
    },
    check?: BooleanModifier | ((a: Attribute) => BooleanModifier)
};

export type extendedAttributeSettings = {
    type: Datatype,
    unique?: boolean,
    autoIncrement?: boolean,
    default?: primaryData,
    notNull?: boolean
    primaryKey?: boolean,
    foreignKey?: {
        link: Attribute,
        onDelete?: onAction,
        onUpdate?: onAction
    },
    check?: BooleanModifier | ((a: Attribute) => BooleanModifier)
}

export enum onAction {
    nothing,
    cascade,
    noAction,
    setNull,
    setDefault
}

export enum joinType {
    inner,
    left,
    right,
    full,
    cross
}

export enum dbType {
    mariadb,
    //mysql,
    postgres,
}

export enum order {
    ASC,
    DESC
}