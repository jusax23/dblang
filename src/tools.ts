import crypto from "crypto";

export const sha256 = (d: any) => crypto.createHash('sha256').update(String(d)).digest('base64');