import { AttributeAlias } from "./alias.js";
import { Attribute, DB, Table } from "./db.js";
import { BooleanModifier, Modifier } from "./dbStructure.js";
import { Handler } from "./defaultHandler.js";
import { allModifierInput, order, primaryData, selectElements, selectFromElements, serializeReturn } from "./types.js";


export class Query {
    sql: string;
    values: primaryData[];
    constructor([sql, values]: serializeReturn) {
        this.sql = sql;
        this.values = values;
    }
}

export class QueryBuilder {
    //injekt and data
    list: ([boolean, primaryData])[] = [];

    constructor(l?: ({ inject?: boolean, data: primaryData })[]) {
        if (Array.isArray(l))
            for (let i = 0; i < l.length; i++) {
                const e = l[i];
                this.list.push([e.inject ? true : false, e.data]);
            }
    }
    addCode(text: string) {
        this.list.push([false, text]);
    }
    addCodeCommaSeperated(data: string[], comma = ", ") {
        for (let i = 0; i < data.length; i++) {
            const e = data[i];
            this.list.push([false, e]);
            if (i + 1 < data.length) this.list.push([false, comma]);
        }
    }
    addInjection(data: primaryData) {
        this.list.push([true, data]);
    }
    addInjectionCommaSeperated(data: primaryData[], comma = ", ") {
        for (let i = 0; i < data.length; i++) {
            const e = data[i];
            this.list.push([true, e]);
            if (i + 1 < data.length) this.list.push([false, comma]);
        }
    }
    append(qb: QueryBuilder) {
        this.list.push(...qb.list);
    }
    appendEnding(qb: QueryBuilder) {
        if (qb.isEmpty()) return;
        this.append(qb);
        this.list.push([false, ";"]);
    }
    isEmpty() {
        return this.list.length == 0;
    }
    /*setHandler(fun:(d:any)=>any){

    }*/
}


export class selectQuery {
    attr: selectElements[] = [];
    from: selectFromElements;
    constructor(a: selectElements[], from: selectFromElements) {
        this.attr.push(...a.flat(Infinity));
        this.from = from ? from : null;
    }
    whereD: BooleanModifier | null = null;
    where(m: BooleanModifier) {
        this.whereD = m;
        return this;
    }
    groupByD: (Attribute | AttributeAlias)[] = [];
    groupBy(a: (Attribute | AttributeAlias)[]) {
        this.groupByD = a;
        return this;
    }
    havingD: BooleanModifier | null = null;
    having(m: BooleanModifier) {
        this.havingD = m;
        return this;
    }

    orderByD: ([allModifierInput, order])[] = [];
    addOrderBy(a: allModifierInput, o: order = order.ASC) {
        this.orderByD.push([a, o]);
        return this;
    }
    orderBY(a: allModifierInput, o: order = order.ASC) {
        this.orderByD = [[a, o]];
        return this;
    }

    limitD: number | null = null;
    limitOffsetD: number | null = null;
    limit(i: number, offset: number = 0) {
        this.limitD = i;
        this.limitOffsetD = offset;
        return this;
    }

    serialize(handler: Handler): QueryBuilder {
        return handler.querys.select(handler, this);
    }

    async query(db: DB, printQuery = false) {
        const handler = db.getHandler();
        const builder = this.serialize(handler);
        const s = handler.builders.query(builder);
        let readResp = await db.query(s, printQuery);
        return handler.responses.readResponse(readResp);
    }
}

export class insertQuery {
    attrs: Attribute[];
    values: primaryData[][] = [];
    select: selectQuery | null = null;
    constructor(attrs: Attribute[]) {
        if (attrs.length == 0) throw new Error("Insertion must be done in at least one Column.");
        for (let i = 0; i < attrs.length; i++) {
            if (attrs[i].table != attrs[0].table) throw new Error("Insertion Columns must be in one Table.");
        }
        this.attrs = attrs;
    }
    add(...data: primaryData[]) {
        if (this.select != null) throw new Error("Can not add Values when using select!");
        this.values.push(data);
        return this;
    }
    addValues(...data: primaryData[][]) {
        if (this.select != null) throw new Error("Can not add Values when using select!");
        this.values.push(...data);
        return this;
    }
    setSelect(state: selectQuery) {
        if (this.values.length != 0) throw new Error("Can not add select when using values!");
        this.select = state;
        return this;
    }
    serialize(handler: Handler): QueryBuilder {
        return handler.querys.insert(handler, this);
    }
    async query(db: DB, printQuery = false) {
        const handler = db.getHandler();
        const builder = this.serialize(handler);
        const s = handler.builders.query(builder);
        let readResp = await db.query(s, printQuery);
        return handler.responses.insertResponse(readResp);
    }
}

export class updateQuery {
    table: Table;
    setD: ([Attribute, allModifierInput])[] = [];
    whereD: BooleanModifier | null = null;

    constructor(table: Table) {
        this.table = table;
    }
    set(attr: Attribute, value: allModifierInput) {
        if (this.table != attr.table) throw new Error("Can only edit columns of the updated table!");
        this.setD.push([attr, value]);
        return this;
    }
    where(w: BooleanModifier) {
        this.whereD = w;
        return this;
    }
    serialize(handler: Handler): QueryBuilder {
        return handler.querys.update(handler, this);
    }
    async query(db: DB, printQuery = false) {
        const handler = db.getHandler();
        const builder = this.serialize(handler);
        const s = handler.builders.query(builder);
        let readResp = await db.query(s, printQuery);
        return handler.responses.writeResponse(readResp);
    }
}

export class removeQuery {
    table: Table;
    whereD: BooleanModifier | null = null;
    constructor(table: Table) {
        this.table = table;
    }
    where(w: BooleanModifier) {
        this.whereD = w;
        return this;
    }
    serialize(handler: Handler): QueryBuilder {
        return handler.querys.remove(handler, this);
    }
    async query(db: DB, printQuery = false) {
        const handler = db.getHandler();
        const builder = this.serialize(handler);
        const s = handler.builders.query(builder);
        let readResp = await db.query(s, printQuery);
        return handler.responses.writeResponse(readResp);
    }
}